from selenium import webdriver
import unittest
from pages.mail_page import Authorisation, MainPage, EmailForm, InformationForm
import logging


class ContactCreation(unittest.TestCase):

    site = 'https://account.mail.ru/'
    login = 'test.avtotest'
    receiver = 'test.avtotest@mail.ru'
    password = 'Quality4321'
    message = 'Скорее бы на работу'
    information_message = 'Письмо отправлено'

    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_argument("--start-maximized")
        self.driver = webdriver.Chrome(options=options)
        self.driver.get(self.site)
        self.driver.implicitly_wait(4)

    def test_01_create_email(self):
        """Отправка email"""

        logging.info('Авторизуемся на сайте')
        authorise = Authorisation(self.driver)
        authorise.authorise(self.login, self.password)

        logging.info('Создаем письмо')
        main_page = MainPage(self.driver)
        main_page.create_new_email()

        logging.info('Заполняем и отправяем письмо')
        email_form = EmailForm(self.driver)
        email_form.fill_email(self.receiver, self.message)
        email_form.send_letter()

        logging.info('Проверяем отчет об отправке')
        information = InformationForm(self.driver)
        information.check_sending(self.information_message)

    def tearDown(self):
        self.driver.close()


if __name__ == '__main__':
    unittest.main()
