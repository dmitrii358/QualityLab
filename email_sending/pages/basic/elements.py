from selenium.webdriver.common.by import By


class Elements:
    """Поиск элементов"""
    def __init__(self, driver, rus_name: str = ''):
        self.driver = driver
        self.rus_name = rus_name

    def find_elements(self, elem):
        """Обертка для поиска элементов

        :param elem: стратегия поиска
        """

        elements = self.driver.find_element(elem['how'], elem['locator'])
        return elements

    @staticmethod
    def element(how=By.CSS_SELECTOR, locator='', rus_name=''):
        """Обертка для поиска элементов

        :param how: стратегия поиска
        :param locator: локатор
        :param rus_name: название
        """

        element = {'how': how, 'locator': locator}
        return element

