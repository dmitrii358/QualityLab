from selenium.webdriver.support.ui import WebDriverWait
from pages.basic.elements import Elements
from selenium.webdriver.support import expected_conditions as EC


class Common(Elements):
    """Базовые методы"""

    def check_presence(self, element, visibility=True):
        """Проверяем отображение элемента на странице

        :param element: элемент, которого ждем
        :param visibility:присутствует или нет
        """

        wait = WebDriverWait(self.driver, 10)
        if visibility:
            wait.until(EC.visibility_of(element), f'Не отображается элемент {self.rus_name}')
        else:
            wait.until(EC.invisibility_of_element(element), f'Элемент {self.rus_name}  не должен отображаться')

    def check_text_in_element(self, element, text):
        """Проверяем наличие текста в элементе"""

        wait = WebDriverWait(self.driver, 1)
        wait.until(EC.text_to_be_present_in_element((element['how'], element['locator']), text),
                   f'Не найден текст в элементе {self.rus_name}')
