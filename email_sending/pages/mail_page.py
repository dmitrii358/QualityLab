from selenium.webdriver.common.by import By
from pages.basic.elements import Elements
from pages.basic.common_methods import Common
import logging


class Authorisation(Elements):
    """Форма авторизации"""

    authorisation_panel      = Elements.element(      By.CSS_SELECTOR,  '.login-panel', 'Форма авторизации')
    login_field              = Elements.element(      By.CSS_SELECTOR,  '[name="username"]', 'Поле логина')
    enter_password           = Elements.element(      By.CSS_SELECTOR,  '[data-test-id="next-button"]', 'Ввести пароль')
    password_field           = Elements.element(      By.CSS_SELECTOR,  '[name="password"]', 'Поле пароля')
    submit                   = Elements.element(      By.CSS_SELECTOR,  '[data-test-id="submit-button"]', 'Войти')

    def authorise(self, login, password):
        """Авторизуемся на сайте

        :param login: логин
        :param password: пароль
        """

        logging.info('Авторизуемся на сайте')
        common_methods = Common(self.driver)
        elements = Elements(self.driver)
        common_methods.check_presence(elements.find_elements(self.authorisation_panel))
        elements.find_elements(self.login_field).click()
        elements.find_elements(self.login_field).send_keys(login)
        elements.find_elements(self.enter_password).click()
        elements.find_elements(self.password_field).send_keys(password)
        elements.find_elements(self.submit).click()


class MainPage(Elements):
    """Главная страница почты"""

    panel        =  Elements.element(     By.CSS_SELECTOR,  '.application-mail__layout_main',  'Главная страница почты')
    create_mail  =  Elements.element(     By.CSS_SELECTOR,  '.compose-button',  'Написать письмо')

    def create_new_email(self):
        """Создаем письмо
        """

        logging.info('Создаем письмо')
        common_methods = Common(self.driver)
        elements = Elements(self.driver)
        common_methods.check_presence(elements.find_elements(self.panel))
        elements.find_elements(self.create_mail).click()


class EmailForm(Elements):
    """Форма письма"""

    panel      =    Elements.element(     By.CSS_SELECTOR,    '.compose-app_window', 'Форма письма')
    whom       =    Elements.element(     By.CSS_SELECTOR,    '.container--hmD9c:not(.disabled--BiBQy) input', 'Кому')
    whom_stick =    Elements.element(     By.CSS_SELECTOR,    '.text--1tHKB', 'Кому')
    email_body =    Elements.element(     By.CSS_SELECTOR,    '.cke_editable div', 'Тело письма')
    send_email =    Elements.element(     By.CSS_SELECTOR,    '.button2_primary', 'Отправить')

    def fill_email(self, receiver, message):
        """Заполняем письмо

        :param receiver: получатель
        :param message: письмо
        """

        logging.info('Заполняем письмо')
        common_methods = Common(self.driver)
        elements = Elements(self.driver)
        common_methods.check_presence(elements.find_elements(self.panel))
        elements.find_elements(self.whom).send_keys(receiver)
        elements.find_elements(self.email_body).click()
        elements.find_elements(self.email_body).send_keys(message)

        logging.info('Проверяем, что поля заполены корректно')
        common_methods.check_text_in_element(self.whom_stick, receiver)
        common_methods.check_text_in_element(self.email_body, message)

    def send_letter(self):
        """Отправляем письмо"""

        logging.info('Отправляем письмо')
        elements = Elements(self.driver)
        elements.find_elements(self.send_email).click()


class InformationForm(Elements):
    """Форма информирования об отправке"""

    information_panel =  Elements.element(    By.CSS_SELECTOR, '.layer-sent-page', 'Панель уведомления')
    information_field =  Elements.element(    By.CSS_SELECTOR, '.layer-sent-page .layer__link', 'Статус отправки')

    def check_sending(self, message):
        """Проверяем уведомление об отправке

        :param message: сообщение в отчете
        """

        logging.info('Проверяем, что появилось уведомление об успешной отправке')
        common_methods = Common(self.driver)
        elements = Elements(self.driver)
        common_methods.check_presence(elements.find_elements(self.information_panel))
        common_methods.check_text_in_element(self.information_field, message)

